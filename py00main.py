# This is a sample Python script.
import py01arithmeticFunctions


# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/

# Crafted code
print(py01arithmeticFunctions.add(5, 2))
print(py01arithmeticFunctions.subtract(5, 2))
print(py01arithmeticFunctions.multiply(5, 2))
divisionList = py01arithmeticFunctions.divide(5, 2)
print(divisionList[0])
print(divisionList[1])
