import unittest
import py01arithmeticFunctions as arithmeticFunctions


class MyTestCase(unittest.TestCase):
    def test_something(self):
        self.assertEqual(True, True)

    def test_Add(self):
        self.assertEqual(0, arithmeticFunctions.add(0, 0))
        self.assertEqual(101, arithmeticFunctions.add(0, 101))
        self.assertEqual(arithmeticFunctions.add(1, 101), 102)

    def test_Subtract(self):
        self.assertEqual(0, arithmeticFunctions.subtract(0, 0))
        self.assertEqual(-101, arithmeticFunctions.subtract(0, 101))
        self.assertEqual(-100, arithmeticFunctions.subtract(1, 101))
        self.assertEqual(0, arithmeticFunctions.subtract(100, 100))
        self.assertEqual(99, arithmeticFunctions.subtract(100, 1))


if __name__ == '__main__':
    unittest.main()
