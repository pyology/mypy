import os

def create_file_in_empty_folders(dirpath):
    for subdir,  dirs, files in os.walk(dirpath):
        dirslength: int = len(dirs)
        fileslength: int = len(files)
        print(dirslength, fileslength)
        if dirslength ==0 and fileslength ==0:
            print(subdir, "empty")
            file1 = open(os.path.join(subdir,"TODO.md"), "w")
            """file1.write("")
            file1.close()"""
            print(file1)

# Function to Check if the path specified
# is a valid directory
def isEmpty(path):
    if os.path.exists(path) and not os.path.isfile(path):

        # Checking if the directory is empty or not
        if not os.listdir(path):
            print("Empty directory")
        else:
            print("Not empty directory")
    else:
        print("The path is either for a file or not valid")


create_file_in_empty_folders("D:\\test")
