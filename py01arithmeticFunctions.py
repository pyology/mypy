def add(addendum1: object, addendum2: object) -> object:
    return addendum1 + addendum2


def subtract(subtrahend: int, minuend: int):
    return subtrahend - minuend


def multiply(multiplicand: int, multiplier: int):
    return multiplicand * multiplier


def divide(dividend, divisor):
    return [dividend / divisor, dividend % divisor]


def power(base: int, exponent: int):
    return base ** exponent
